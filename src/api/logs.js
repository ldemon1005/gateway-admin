import axios from '@/utils/axios'

export function listJobs(query) {
  return axios({
    url: '/v46.0/gw/job/list',
    method: 'get',
    params: query
  })
}

export function detailJobById(id) {
  console.log(id)
  return axios({
    url: '/jobs-api/job/' + id,
    method: 'get'
  })
}

export function listLogs(query) {
  return axios({
    url: '/v46.0/gw/log/list',
    method: 'get',
    params: query
  })
}

export function listMemory(query) {
  return axios({
    url: '/v46.0/gw/memory/list',
    method: 'get',
    params: query
  })
}

export function listTracking(query) {
  return axios({
    url: '/v46.0/gw/tracking/list',
    method: 'get',
    params: query
  })
}

export function listCaseMpit(query) {
  return axios({
    url: '/v46.0/gw/case-mpit/list',
    method: 'get',
    params: query
  })
}



